package test;

import static org.junit.Assert.*;

import org.junit.Test;

import XML.ChargeurMagasin;

public class testChargeurMagasin {
	
	ChargeurMagasin mag = new ChargeurMagasin("musicbrainzSimple");

	@Test
	public void testChargerMagasin() {
		assertEquals("Le magasin n'est pas correct","musicbrainzSimple",mag.chargerMagasin);
	}

}
